const baseUrl = process.env.REACT_APP_API_BASE_URL;

export async function getProducts(category) {
  const response = await fetch(baseUrl + "trips");
  if (response.ok) return response.json();
  throw response;
}