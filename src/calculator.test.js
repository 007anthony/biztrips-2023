const calculator = require('./calculator');
test('adds 1 to 2 equal 3', () => {
    expect(calculator.sum(1, 2)).toBe(3)
})


test('add with negative numbers', () => {
    expect(calculator.add(-10, -40)).toBe(-50)
})


test('subtracts 3 minus 2 equal 1', () => {
    expect(calculator.sub(3, 2)).toBe(1)
})

test('subtract with negative numbers', () => {
    expect(calculator.sub(20, -10)).toBe(30)
})

test('multiply 6 with 2 equals 12', () => {
    expect(calculator.mul(6, 2)).toBe(12)
})

test('multiply with negative number', () => {
    expect(calculator.mul(-5, 5)).toBe(-25)
})

test('divide 10 with 2 equals 5', () => {
    expect(calculator.div(10, 2)).toBe(5)
})

test('divide with 0', () => {
    expect(calculator.div(0, 10)).toBe(0)
})

test('divide with floating number', () => {
    expect(calculator.div(1.25, 2)).toBeCloseTo(0.625)
})

